package prjava00;

import java.net.*;

/**
 *
 * @author alroga
 */ 
public class Prjava00 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hola Mon");
        System.out.println("Versio 0.1 del projecte prjava00");
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String ipAddr = addr.getHostAddress();
            String hostname = addr.getHostName();
            System.out.println("hostname = " + hostname);
            System.out.println("Adreça IP: " + ipAddr);
            System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
            System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
            System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
            System.out.println("Versio OS: " + System.getProperty("os.version"));
            
        }catch (UnknownHostException e){
            e.printStackTrace();
        }
        
    }
        
}
